# [purnet-lab.gitlab.io](https://purnet-lab.gitlab.io) repository

The repository contians the metadata and accompanying files for setting up the PurNET lab webpage. Below is a collection of How-Tos to add headshots and to modify the various pages of the PurNET website.

[[_TOC_]]

## Adding a headshot

For creating a headshot you will need to follow these basic steps.

### a. Clone the repositories

```
$ cd ~
$ git clone https://gitlab.com/purnet-lab/purnet-lab.gitlab.io.git  # Clone the main repo
$ cd ~
$ git clone https://gitlab.com/purnet-lab/purnet-lab.gitlab.io.wiki.git  # Clone the wiki repo
```

### b. Create the headshot PNG image

1. Use the appropriate PowerPoint file under the `~/purnet-lab.gitlab.io/others/images/faces` folder (e.g., `faculty.pptx` or `students.pptx`) to generate a headshot slide. Here's an example figure:

> **Note:** Make sure to crop and size your image according to the other headshots, otherwise, they will appear in a haphazard way on the main wiki page. You can follow this article on how to crop images in PowerPoint, [link](https://support.microsoft.com/en-us/office/video-crop-a-picture-to-fit-a-shape-d9ed38c2-1006-4245-9eec-9dd4f722986d).

<img src="others/images/guides/headshot.png" alt="Headshot." width="800">

2. Next, save your headshot slide as a PNG image using the settings shown in the following figure:
  > **Note:** The *width/height* export option may be missing on Windows. In that case, use the following instructions. Select the slide containing your headshot -> `File` -> `Export` -> `Change File Type` -> ``PNG Portable Network Graphics (*.png)`` -> In the *Save As* dialog -> `Tools` -> `Compress Pictures` -> `HD (330 PPI)` -> `Save` -> Open the saved image using the `Photos` app -> Open the three-dot drop down menu on the top-right -> `Resize` -> `Define custom dimensions` -> Enter `1024 x 1463` -> `Save resized image`

> **Note:** PNG image, **width/height** = **1024/1463**.

<img src="others/images/guides/save-as-png.png" alt="Save as PNG." width="800">

3. Copy the PNG image in the corresponing `faculty`/`students` folder under the  `~/purnet-lab.gitlab.io.wiki/images` path.

4. Push your changes to the repositories. (Feel free to push your modifed PowerPoint file as well.)

### c. Displaying the headshot on the main wiki page

- Insert the link to your PNG in the `~/purnet-lab.gitlab.io.wiki/Home.md` file, using one of the two ways:
  - If you are editing the file locally on your machine, remember to push your changes to the repo.
  - Otherwise, you can also modify the `Home.md` file online using the GitLab [editor][https://gitlab.com/purnet-lab/purnet-lab.gitlab.io/-/wikis/Home/edit]. 

- Here's an example of how to insert the link; append it under the appropriate `students`/`faculty` section in the `Home.md` file.

```
&nbsp;&nbsp;&nbsp;
[<img src="images/students/jane-doe.png" alt="Jane Doe" width="160"/>](<your personal URL>)
```

That's it. Enjoy!

## Editing wiki pages

You can edit PurNET lab webpages (i.e., wiki pages) online using the GitLab editor. 

To do so, visit a page and then press the edit button (see an example in the figure below) to open the editor. Once done, press the `Save changes` button in blue at the bottom of the page to commit your changes.

<img src="others/images/guides/editing-example.png" alt="Editing Example." width="800">
